<?php
namespace Kowal\FacebookPixelCode\Block;

use Magento\Customer\CustomerData\SectionSourceInterface;

class AddToCart implements SectionSourceInterface
{
    /**
     * @var \Kowal\FacebookPixelCode\Model\SessionFactory
     */
    protected $fbPixelSession;

    /**
     * AddToCart constructor.
     * @param \Kowal\FacebookPixelCode\Model\SessionFactory $fbPixelSession
     */
    public function __construct(
        \Kowal\FacebookPixelCode\Model\SessionFactory $fbPixelSession
    ) {
        $this->fbPixelSession = $fbPixelSession;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getSectionData()
    {
        $data = [
            'events' => []
        ];

        if ($this->fbPixelSession->create()->hasAddToCart()) {
            // Get the add-to-cart information since it's unique to the user
            // but might be displayed on a cached page
            $data['events'][] = [
                'eventName' => 'AddToCart',
                'eventAdditional' => $this->fbPixelSession->create()->getAddToCart()
            ];
        }
        return $data;
    }
}

<?php

namespace Kowal\FacebookPixelCode\Observer;

use Magento\Framework\Event\ObserverInterface;

class WishlistAddProduct implements ObserverInterface
{

    /**
     * @var \Kowal\FacebookPixelCode\Model\SessionFactory
     */
    protected $fbPixelSession;

    /**
     * @var \Kowal\FacebookPixelCode\Helper\Data
     */
    protected $helper;

    /**
     * WishlistAddProduct constructor.
     * @param \Kowal\FacebookPixelCode\Model\SessionFactory $fbPixelSession
     * @param \Kowal\FacebookPixelCode\Helper\Data $helper
     */
    public function __construct(
        \Kowal\FacebookPixelCode\Model\SessionFactory $fbPixelSession,
        \Kowal\FacebookPixelCode\Helper\Data $helper
    ) {
        $this->fbPixelSession = $fbPixelSession;
        $this->helper        = $helper;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return boolean
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $product = $observer->getItem()->getProduct();
        /** @var \Magento\Catalog\Model\Product $product */
        if (!$this->helper->isAddToWishList() || !$product) {
            return true;
        }
        
        $data = [
            'content_type' => 'product',
            'content_ids' => $product->getSku(),
            'content_name' => $product->getName(),
            'currency' => $this->helper->getCurrencyCode()
        ];

        $this->fbPixelSession->create()->setAddToWishlist($data);
        
        return true;
    }
}

<?php

namespace Kowal\FacebookPixelCode\Observer;

use Magento\Framework\Event\ObserverInterface;

class Search implements ObserverInterface
{
    /**
     * @var \Kowal\FacebookPixelCode\Model\SessionFactory
     */
    protected $fbPixelSession;
    /**
     * @var \Kowal\FacebookPixelCode\Helper\Data
     */
    protected $fbPixelHelper;
    /**
     * @var \Magento\Search\Helper\Data
     */
    protected $searchHelper;
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * Search constructor.
     * @param \Kowal\FacebookPixelCode\Helper\Data $helper
     * @param \Magento\Search\Helper\Data $searchHelper
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Kowal\FacebookPixelCode\Model\SessionFactory $fbPixelSession
     */
    public function __construct(
        \Kowal\FacebookPixelCode\Helper\Data $helper,
        \Magento\Search\Helper\Data $searchHelper,
        \Magento\Framework\App\RequestInterface $request,
        \Kowal\FacebookPixelCode\Model\SessionFactory $fbPixelSession
    ) {
        $this->fbPixelSession = $fbPixelSession;
        $this->fbPixelHelper         = $helper;
        $this->searchHelper = $searchHelper;
        $this->request = $request;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return boolean
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $text = $this->searchHelper->getEscapedQueryText();
        if (!$text) {
            $text = $this->request->getParams();
            foreach ($this->request->getParams() as $key => $value) {
                $text[$key] = $value;
            }
        }
        if (!$this->fbPixelHelper->isSearch() || !$text) {
            return true;
        }

        $data = [
            'search_string' => $text
        ];
        $this->fbPixelSession->create()->setSearch($data);

        return true;
    }
}

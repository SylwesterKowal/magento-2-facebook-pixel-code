<?php

namespace Kowal\FacebookPixelCode\Observer;

use Magento\Framework\Event\ObserverInterface;

class Register implements ObserverInterface
{

    /**
     * @var \Kowal\FacebookPixelCode\Model\SessionFactory
     */
    protected $fbPixelSession;

    /**
     * @var \Kowal\FacebookPixelCode\Helper\Data
     */
    protected $fbPixelHelper;

    /**
     * Register constructor.
     * @param \Kowal\FacebookPixelCode\Model\SessionFactory $fbPixelSession
     * @param \Kowal\FacebookPixelCode\Helper\Data $helper
     */
    public function __construct(
        \Kowal\FacebookPixelCode\Model\SessionFactory $fbPixelSession,
        \Kowal\FacebookPixelCode\Helper\Data $helper
    ) {
        $this->fbPixelSession = $fbPixelSession;
        $this->fbPixelHelper  = $helper;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return boolean
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $customer = $observer->getEvent()->getCustomer();
        if (!$this->fbPixelHelper->isRegistration()
            || !$customer
        ) {
            return true;
        }
        $data = [
            'customer_id' => $customer->getId(),
            'email' => $customer->getEmail(),
            'fn' => $customer->getFirstName(),
            'ln' => $customer->getLastName()
        ];

        $this->fbPixelSession->create()->setRegister($data);

        return true;
    }
}

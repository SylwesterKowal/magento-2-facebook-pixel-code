<?php

namespace Kowal\FacebookPixelCode\Observer;

use Magento\Framework\Event\ObserverInterface;

class Subcribe implements ObserverInterface
{
    /**
     * @var \Kowal\FacebookPixelCode\Model\SessionFactory
     */
    protected $fbPixelSession;

    /**
     * @var \Kowal\FacebookPixelCode\Helper\Data
     */
    protected $helper;

    /**
     * Subcribe constructor.
     * @param \Kowal\FacebookPixelCode\Model\SessionFactory $fbPixelSession
     * @param \Kowal\FacebookPixelCode\Helper\Data $helper
     */
    public function __construct(
        \Kowal\FacebookPixelCode\Model\SessionFactory $fbPixelSession,
        \Kowal\FacebookPixelCode\Helper\Data $helper
    ) {
        $this->fbPixelSession = $fbPixelSession;
        $this->helper        = $helper;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return boolean
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $email = $observer->getEvent()->getSubscriber()->getSubscriberEmail();
        $subscribeId =$observer->getEvent()->getSubscriber()->getSubscriberId();
        if (!$this->helper->isSubscribe() || !$email) {
            return true;
        }

        $data = [
            'id' => $subscribeId,
            'email' => $observer->getEvent()->getSubscriber()->getSubscriberEmail()
        ];

        $this->fbPixelSession->create()->setAddSubscribe($data);

        return true;
    }
}

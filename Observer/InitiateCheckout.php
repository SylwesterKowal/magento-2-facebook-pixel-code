<?php

namespace Kowal\FacebookPixelCode\Observer;

use Magento\Framework\Event\ObserverInterface;

class InitiateCheckout implements ObserverInterface
{

    /**
     * @var \Kowal\FacebookPixelCode\Model\SessionFactory
     */
    protected $fbPixelSession;

    /**
     * @var \Magento\Checkout\Model\SessionFactory
     */
    protected $checkoutSession;

    /**
     * @var \Kowal\FacebookPixelCode\Helper\Data
     */
    protected $fbPixelHelper;

    /**
     * @var \Magento\Framework\Pricing\Helper\Data
     */
    protected $dataPrice;

    /**
     * InitiateCheckout constructor.
     * @param \Magento\Checkout\Model\SessionFactory $checkoutSession
     * @param \Kowal\FacebookPixelCode\Helper\Data $helper
     * @param \Kowal\FacebookPixelCode\Model\SessionFactory $fbPixelSession
     * @param \Magento\Framework\Pricing\Helper\Data $dataPrice
     */
    public function __construct(
        \Magento\Checkout\Model\SessionFactory $checkoutSession,
        \Kowal\FacebookPixelCode\Helper\Data $helper,
        \Kowal\FacebookPixelCode\Model\SessionFactory $fbPixelSession,
        \Magento\Framework\Pricing\Helper\Data $dataPrice
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->fbPixelHelper         = $helper;
        $this->fbPixelSession = $fbPixelSession;
        $this->dataPrice = $dataPrice;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return boolean
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (!$this->fbPixelHelper->isInitiateCheckout()) {
            return true;
        }
        $checkout = $this->checkoutSession->create();
        if (empty($checkout->getQuote()->getAllVisibleItems())) {
            return true;
        }

        $product = [
            'content_ids' => [],
            'contents' => [],
            'value' => "",
            'currency' => ""
        ];
        $items = $checkout->getQuote()->getAllVisibleItems();
        foreach ($items as $item) {
            $product['contents'][] = [
                'id' => $item->getSku(),
                'name' => $item->getName(),
                'quantity' => $item->getQty(),
                'item_price' => $this->dataPrice->currency($item->getPrice(), false, false)
            ];
            $product['content_ids'][] = $item->getSku();
        }
        $data = [
            'content_ids' => $product['content_ids'],
            'contents' => $product['contents'],
            'content_type' => 'product',
            'value' => $checkout->getQuote()->getGrandTotal(),
            'currency' => $this->fbPixelHelper->getCurrencyCode(),
        ];
        $this->fbPixelSession->create()->setInitiateCheckout($data);

        return true;
    }
}
